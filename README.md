# Installation
1. Extract the SDK to a folder named libs under your project root.
2. Add these lines to your build.gradle (Module: (app)...)
    ```gradle
    dependencies {
        implementation fileTree(dir: 'libs', include: ['*.jar']) 
        implementation 'com.parse:parse-android:1.15.8'
        implementation(name: 'liphy-sdk', ext: 'aar')
    }
    ```
3. Add these lines to your build.gradle (Project: ...)
    ```gradle
    allprojects { 
        repositories{
            ... 
            flatDir {
                dirs ‘libs’ 
            }
        } 
    }
    ```
4. Add <uses-permission android:name="android.permission.CAMERA" /> to your AndroidManifest.xml for gaining access to the camera.
5. Add a TextureView in the layout 
    ```xml
    <TextureView 
        android:layout_width=“match_parent” 
        android:layout_height=“match_parent" android:id="@+id/textureView"/>
    ```


# Usage

```java
implements LightFlyCamera.OnLightTrackedCallback

LightFlyCamera lightFlyManager;
lightFlyManager = LightFlyCamera.getCameraInstance(this); lightFlyManager.setLightTrackedCallback(this); lightFlyManager.setTextureView((TextureView)findViewById(R.id.textureView)); lightFlyManager.setAcessKeyForSDK(“// put your access key here”); lightFlyManager.startTracking();

@Override public void didTrackLight(final String lightId){
    // Do the light detection related events here
}
```
 
 
#  Classes & Protocols

### LightFlyCamera
- The LightFlyCamera object is the entry point to the VLC light service.
- `startTracking`: Start tracking VLC light.
    ```
    // Declaration
    
    // java
    final void startTracking()
    ```
    
- `stopTracking`: To stop tracking the VLC Lights.
    ```
    // Declaration
    
    // java
    void stopTracking();
    ```

- `didTrackLight(String lightId)`: This is invoked when a VLC light is in the vision of camera.

    ```
    // Declaration
    
    // java
    void didTrackLight(String lightId);
    LiPHY Communications Ltd. Confidential and Proprietary
    ```
  
