package liphy.io.liphysample;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import liphy.io.liphysdk.LightFlyCamera;

public class MainActivity extends AppCompatActivity implements LightFlyCamera.OnLightTrackedCallback, CompoundButton.OnCheckedChangeListener{


    private LightFlyCamera lightFlyManager;
    final Handler mHandler = new Handler();
    TextView tvLightID;
    TextView tvSDKversion;
    TextView tvBuildNumber;
    TextView tvExpiryDate;
    boolean isFrontSide = false;
    Switch swCamera;
    //SeekBar sdkSwitchSeekbar;
    //TextView tvSDKtype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvLightID = findViewById(R.id.tvLightID);
        tvSDKversion = findViewById(R.id.tvSDKversion);
        tvExpiryDate = findViewById(R.id.tvExpiryDate);
        tvBuildNumber = findViewById(R.id.tvBuildNumber);
        swCamera = findViewById(R.id.swCamera);

        // Setup LiPHY SDK configuration
        lightFlyManager = LightFlyCamera.getCameraInstance(this);
        lightFlyManager.setLightTrackedCallback(this);
        lightFlyManager.setTextureView((TextureView) findViewById(R.id.textureView));
        lightFlyManager.setAcessKeyForSDK("...Put the key here (optional)..");
        lightFlyManager.useFrontCamera(false); // False = Backside, True = Frontside

        tvSDKversion.setText("SDK Version: "+lightFlyManager.getSDKversion());
        tvExpiryDate.setText("Expiry Date: "+lightFlyManager.getExpiryDate());
        tvBuildNumber.setText("Build Number: "+lightFlyManager.getSDKBuildNumber());

        swCamera.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        lightFlyManager.stopTracking();

        if (isChecked) {

            swCamera.setText("Frontside");
            isFrontSide = true;


        } else {
            swCamera.setText("Backside");
            isFrontSide = false;


        }

        lightFlyManager.useFrontCamera(isFrontSide);
        lightFlyManager.startTracking();

    }

    @Override
    protected void onResume() {
        super.onResume();

        // Start Tracking for LiPHY
        lightFlyManager.startTracking();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Stop Tracking for LiPHY
        lightFlyManager.stopTracking();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Stop Tracking for LiPHY
        lightFlyManager.stopTracking();
    }

    @Override
    public void didTrackLight(final String light, boolean isFrontSide){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvLightID.setText("You are scanning Light: " + light);
            }
        });
    }
}
